const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const authenticate = require('../authenticate');
const Favorites = require('../models/favorite');
const cors = require('./cors');
const favorites = express.Router();

favorites.use(bodyParser.json());

favorites.route('/')
.get(cors.cors,authenticate.verifyUser, (req,res,next) => {
    console.log(req.user);
    Favorites.findOne({'user' : req.user})
    .populate('user')
    .populate('dishes')
    .then((favorites) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorites);
    }, (err) => next(err))
    .catch((err) => next(err));
})
.post(cors.corsWithOptions,authenticate.verifyUser, (req, res, next) => {
    
    Favorites.findOne({'user' : req.user}, (err, results) =>{
        if(err){
            next(err);
        }
        if(!results){
            Favorites.create({
                user: req.user,
                dishes: req.body
            })
            .then((favorite) => {
                console.log('Dish Created ', favorite);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorite);
            }, (err) => next(err))
            .catch((err) => next(err));
        }
        else{
            Favorites.findOne({'user': req.user})
            .then((favorite)=>{
                console.log("Was here");
                for(var i=0 ; i < req.body.length; i++){
                    if(!favorite.dishes.id(req.body[i])){
                    favorite.dishes.push(req.body);
                    }
                }
                favorite.save().then((dish) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(dish);                
                }, (err) => next(err));
            },(err) => next(err)).catch((err) => next(err));
        }
    });

})
.delete(cors.corsWithOptions,authenticate.verifyUser,(req, res, next) => {
    Favorites.findOneAndRemove({'user' : req.user})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));    
});

favorites.route('/:dishId')
.get(cors.cors, authenticate.verifyUser, (req,res,next) => {
    Favorites.findOne({user: req.user._id})
    .then((favorites) => {
        if (!favorites) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json({"exists": false, "favorites": favorites});
        }
        else {
            if (favorites.dishes.indexOf(req.params.dishId) < 0) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "favorites": favorites});
            }
            else {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": true, "favorites": favorites});
            }
        }

    }, (err) => next(err))
    .catch((err) => next(err))
})
.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    Favorites.findOne({'user' : req.user}, (err, results)=> {
        if(err){
            next(err);
        }
        else if(!results){
            console.log('Was here');
            if(!results.dishes.id(req.params.dishId)){
            Favorites.create({
                "user": req.user,
                "dishes": req.params.dishId
            })
            .then((dish) => {
                console.log('Dish Created ', dish);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(dish);
            }, (err) => next(err)).catch((err) => next(err))
        }
    }
    else{
        results.dishes.push(req.params.dishId);
        results.save()
        .then((result)=>{
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(result);
        }, (err) => next(err));
        
    }
    }).catch((err) => next(err));
    
})
.delete(cors.corsWithOptions,authenticate.verifyUser,(req, res, next) => {
    Favorites.findOne({'user' : req.user}, (err, results) =>{
        if(err){
            next(err);
        }
        else if(!results){
            res.statusCode = 404;
            res.end('You dont have any dishes in your collection');
        }
        else{
            if(results.dishes.indexOf(req.params.dishId) < 0){
                res.statusCode = 404;
                res.end('You dont have this dish in your collection');
            }
            else{
                var arr = results.dishes.slice(0,results.dishes.indexOf(req.params.dishId))+
                results.dishes.slice(results.dishes.indexOf(req.params.dishId)+1);
                results.dishes = arr;
                results.save()
                .then((favorite) => {
                    Favorites.findById(favorite._id)
                    .populate('user')
                    .populate('dishes')
                    .then((favorite) => {
                        console.log('Favorite Dish Deleted!', favorite);
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(favorite);
                    })
                })
            }
            
        }
    }).catch((err) => next(err));
    
});


module.exports = favorites;